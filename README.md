# An evaluation of machine learning approaches for the prediction of essential genes in eukaryotes using protein sequence-derived features (Computational and Structural Biotechnology Journal, 2019) 

Tulio L. Campos, Pasi K. Korhonen, Robin B. Gasser, Neil D. Young

Link to publication: https://www.sciencedirect.com/science/article/pii/S2001037019301357

#

**Data**: essential_data directory

**Script used for essential gene predictions within a species and among species**: see Caret_FS_REAL_OGEE.R

**Script used for the leave-one-out approach**: see Caret_ALL_FS_REAL_OGEE.R

Inputs for the ML methods are two multi-FASTA files containing protein sequences, one with essential genes and another one with non-essential genes:

BARPLOT.R - generates barplot with data summary;
GO_analysis.R - Gene ontology analysis of essential genes;
Upset_graph.R - Ortholog identifiers (from OMA orthology database) shared among datasets of essential genes


