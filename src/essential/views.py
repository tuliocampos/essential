import random
import operator

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import TemplateView
from blog.models import Post
from .models import Species,Sequence,Features_protr_Composition,Features_protr_Correlation,Features_protr_CTDTriad,Features_protr_PseudoComposition,Features_protr_QSO,Features_rdnase,Features_external_programs,Genes,Phenotypes
from .models import Phenotype_SGD,Phenotype_PomBase,Phenotype_TriTrypDB,Phenotype_GenomeCRISPR,Phenotype_WormBase,Phenotype_SeedGenes,Phenotype_ZFIN,Phenotype_ToxoDB,Phenotype_FlyBase,Phenotype_MGI,Alleles_PomBase,Ontology_WormBase,Ontology_MGI
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape

import django_tables2 as tables
from django_tables2.export.views import ExportMixin
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin,MultiTableMixin

from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
from .tables import SpeciesTable,SequenceTable,GenesTable,GenesTableSearch,SequenceTableSearch,PhenotypesTable,FeaturesProteinsCompositionTable,FeaturesProteinsCorrelationTable,FeaturesProteinsCTDTriadTable,FeaturesProteinsPseudoCompositionTable,FeaturesProteinsQSOTable,FeaturesrDNASeTable,Features_external_programs_Table,Features_immuno_programs_Table
from .tables import Phenotypes_SGD_Table,Phenotypes_PomBase_Table,Phenotypes_TriTrypDB_Table,Phenotypes_GenomeCRISPR_Table,Phenotypes_WormBase_Table,Phenotypes_SeedGenes_Table,Phenotypes_ZFIN_Table,Phenotypes_ToxoDB_Table,Phenotypes_FlyBase_Table,Phenotypes_MGI_Table,Ontology_WormBase_Table,Ontology_MGI_Table
from .filters import GenesListFilter,SequenceListFilter,PhenotypeSGDListFilter,PhenotypePomBaseListFilter,PhenotypeTriTrypDBListFilter,PhenotypeGenomeCRISPRListFilter,PhenotypeWormBaseListFilter,PhenotypeSeedGenesListFilter,PhenotypeZFINListFilter,PhenotypeToxoDBListFilter,PhenotypeFlyBaseListFilter,PhenotypeMGIListFilter,OntologyWormBaseListFilter,OntologyMGIListFilter
from .utils import PagedFilteredTableView
from .forms import GenesListFormHelper,SequenceListFormHelper,PhenotypeSGDListFormHelper,PhenotypePomBaseListFormHelper,PhenotypeTriTrypDBListFormHelper,PhenotypeGenomeCRISPRListFormHelper,PhenotypeWormBaseListFormHelper,PhenotypeSeedGenesListFormHelper,PhenotypeZFINListFormHelper,PhenotypeToxoDBListFormHelper,PhenotypeFlyBaseListFormHelper,PhenotypeMGIListFormHelper,OntologyWormBaseListFormHelper,OntologyMGIListFormHelper

def home(request):
	posts = Post.objects.order_by('published_date')
	return render(request, "base.html", {'posts': posts })

def contact(request):
        return render(request, "contact.html", {})

def predictor(request):
        return render(request, "predictor.html", {})

def test(request):
        return render(request, "test.html", {})

def essentiality(request):
        return render(request, "essentiality.html", {})

def species(request):
        table_species=SpeciesTable(Species.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_species)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_species)
         return exporter.response('table_species.{}'.format(export_format))  
        return render(request,'species_p.html', locals())

def features_protr_composition(request):
#	table_features=FeaturesProteinsCompositionTable(Features_protr_Composition.objects.values('seq_id','features_aac','features_dc','features_tc'))
	table_features=FeaturesProteinsCompositionTable(Features_protr_Composition.objects.all())
	RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
	export_format = request.GET.get('_export', None)
	if TableExport.is_valid_format(export_format):
	 exporter = TableExport(export_format, table_features)
	 return exporter.response('table_features.{}'.format(export_format))
	return render(request,'features_p.html', locals())

def features_protr_correlation(request):
        table_features=FeaturesProteinsCorrelationTable(Features_protr_Correlation.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_protr_ctdtriad(request):
        table_features=FeaturesProteinsCTDTriadTable(Features_protr_CTDTriad.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_protr_pseudocomposition(request):
        table_features=FeaturesProteinsPseudoCompositionTable(Features_protr_PseudoComposition.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_protr_qso(request):
        table_features=FeaturesProteinsQSOTable(Features_protr_QSO.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_rdnase(request):
        table_features=FeaturesrDNASeTable(Features_rdnase.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_external_programs(request):
        table_features=Features_external_programs_Table(Features_external_programs.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def features_immuno_programs(request):
        table_features=Features_immuno_programs_Table(Features_external_programs.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_features)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_features)
         return exporter.response('table_features.{}'.format(export_format))
        return render(request,'features_p.html', locals())

def sequences(request):
        table_sequences=SequenceTable(Sequence.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_sequences)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_sequences)
         return exporter.response('table_sequences.{}'.format(export_format))
        return render(request,'sequences_p.html', locals())

def genes(request):
        table_genes=GenesTable(Genes.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_genes)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_genes)
         return exporter.response('table_genes.{}'.format(export_format))
        return render(request,'genes_p.html', locals())


def phenotypes(request):
        table_phenotype_wormbase=PhenotypesTable(Phenotypes.objects.all())
        RequestConfig(request, paginate={'per_page': 20}).configure(table_phenotype_wormbase)
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
         exporter = TableExport(export_format, table_phenotype_wormbase)
         return exporter.response('table_phenotypes.{}'.format(export_format))
        return render(request,'phenotypes_p.html', locals())


class GenesListView(ExportMixin, PagedFilteredTableView):
    model = Genes
    template_name = 'genes_p.html'
    context_object_name = 'genes'
    ordering = ['gene_id']
    table_class = GenesTable
    filter_class = GenesListFilter
    formhelper_class = GenesListFormHelper

    def get_queryset(self):
        qs = super(GenesListView, self).get_queryset()
        return qs
    
    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(GenesListView, self).get_context_data(**kwargs)
        context['nav_genes'] = True
        search_query = self.get_queryset()
        table_genes = GenesTable(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_genes)
        context['table_genes'] = table_genes
        return context

class SequenceListView(ExportMixin, PagedFilteredTableView):
    model = Sequence
    template_name = 'sequences_p.html'
    context_object_name = 'sequences'
    ordering = ['sequence_id']
    table_class = SequenceTable
    filter_class = SequenceListFilter
    formhelper_class = SequenceListFormHelper

    def get_queryset(self):
        qs = super(SequenceListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(SequenceListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_sequences = SequenceTable(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_sequences)
        context['table_sequences'] = table_sequences
        return context


class PhenotypeSGDListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_SGD
    template_name = 'phenotypes_sgd.html'
    context_object_name = 'phenotype_sgd'
    ordering = ['gene']
    table_class = Phenotypes_SGD_Table
    filter_class = PhenotypeSGDListFilter
    formhelper_class = PhenotypeSGDListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeSGDListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeSGDListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_sgd = Phenotypes_SGD_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_sgd)
        context['table_phenotypes_sgd'] = table_phenotypes_sgd
        return context

class PhenotypePomBaseListView(ExportMixin, PagedFilteredTableView):
    model = Alleles_PomBase
    template_name = 'phenotypes_pombase.html'
    context_object_name = 'phenotype_pombase'
    ordering = ['gene']
    table_class = Phenotypes_PomBase_Table
    filter_class = PhenotypePomBaseListFilter
    formhelper_class = PhenotypePomBaseListFormHelper

    def get_queryset(self):
        qs = super(PhenotypePomBaseListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypePomBaseListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_pombase = Phenotypes_PomBase_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_pombase)
        context['table_phenotypes_pombase'] = table_phenotypes_pombase
        return context

class PhenotypeTriTrypDBListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_TriTrypDB
    template_name = 'phenotypes_tritrypdb.html'
    context_object_name = 'phenotype_tritrypdb'
    ordering = ['gene']
    table_class = Phenotypes_TriTrypDB_Table
    filter_class = PhenotypeTriTrypDBListFilter
    formhelper_class = PhenotypeTriTrypDBListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeTriTrypDBListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeTriTrypDBListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_tritrypdb = Phenotypes_TriTrypDB_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_tritrypdb)
        context['table_phenotypes_tritrypdb'] = table_phenotypes_tritrypdb
        return context


class PhenotypeGenomeCRISPRListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_GenomeCRISPR
    template_name = 'phenotypes_genomecrispr.html'
    context_object_name = 'phenotype_genomecrispr'
    ordering = ['gene']
    table_class = Phenotypes_GenomeCRISPR_Table
    filter_class = PhenotypeGenomeCRISPRListFilter
    formhelper_class = PhenotypeGenomeCRISPRListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeGenomeCRISPRListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeGenomeCRISPRListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_genomecrispr = Phenotypes_GenomeCRISPR_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_genomecrispr)
        context['table_phenotypes_genomecrispr'] = table_phenotypes_genomecrispr
        return context

class PhenotypeWormBaseListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_WormBase
    template_name = 'phenotypes_wormbase.html'
    context_object_name = 'phenotype_wormbase'
    ordering = ['gene']
    table_class = Phenotypes_WormBase_Table
    filter_class = PhenotypeWormBaseListFilter
    formhelper_class = PhenotypeWormBaseListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeWormBaseListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeWormBaseListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_wormbase = Phenotypes_WormBase_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_wormbase)
        context['table_phenotypes_wormbase'] = table_phenotypes_wormbase
        return context


class OntologyWormBaseListView(ExportMixin, PagedFilteredTableView):
    model = Ontology_WormBase
    template_name = 'phenotypes_wormbase.html'
    context_object_name = 'ontology_wormbase'
    ordering = ['wbphenotype']
    table_class = Ontology_WormBase_Table
    filter_class = OntologyWormBaseListFilter
    formhelper_class = OntologyWormBaseListFormHelper

    def get_queryset(self):
        qs = super(OntologyWormBaseListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(OntologyWormBaseListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_wormbase = Ontology_WormBase_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_wormbase)
        context['table_phenotypes_wormbase'] = table_phenotypes_wormbase
        return context


class PhenotypeSeedGenesListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_SeedGenes
    template_name = 'phenotypes_seedgenes.html'
    context_object_name = 'phenotype_seedgenes'
    ordering = ['gene']
    table_class = Phenotypes_SeedGenes_Table
    filter_class = PhenotypeSeedGenesListFilter
    formhelper_class = PhenotypeSeedGenesListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeSeedGenesListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeSeedGenesListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_seedgenes = Phenotypes_SeedGenes_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_seedgenes)
        context['table_phenotypes_seedgenes'] = table_phenotypes_seedgenes
        return context

class PhenotypeZFINListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_ZFIN
    template_name = 'phenotypes_zfin.html'
    context_object_name = 'phenotype_zfin'
    ordering = ['zfin_id']
    table_class = Phenotypes_ZFIN_Table
    filter_class = PhenotypeZFINListFilter
    formhelper_class = PhenotypeZFINListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeZFINListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeZFINListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_zfin = Phenotypes_ZFIN_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_zfin)
        context['table_phenotypes_zfin'] = table_phenotypes_zfin
        return context

class PhenotypeToxoDBListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_ToxoDB
    template_name = 'phenotypes_toxodb.html'
    context_object_name = 'phenotype_toxodb'
    ordering = ['gene']
    table_class = Phenotypes_ToxoDB_Table
    filter_class = PhenotypeToxoDBListFilter
    formhelper_class = PhenotypeToxoDBListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeToxoDBListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeToxoDBListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_toxodb = Phenotypes_ToxoDB_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_toxodb)
        context['table_phenotypes_toxodb'] = table_phenotypes_toxodb
        return context

class PhenotypeFlyBaseListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_FlyBase
    template_name = 'phenotypes_flybase.html'
    context_object_name = 'phenotype_flybase'
    ordering = ['gene']
    table_class = Phenotypes_FlyBase_Table
    filter_class = PhenotypeFlyBaseListFilter
    formhelper_class = PhenotypeFlyBaseListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeFlyBaseListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeFlyBaseListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_flybase = Phenotypes_FlyBase_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_flybase)
        context['table_phenotypes_flybase'] = table_phenotypes_flybase
        return context

class PhenotypeMGIListView(ExportMixin, PagedFilteredTableView):
    model = Phenotype_MGI
    template_name = 'phenotypes_mgi.html'
    context_object_name = 'phenotype_mgi'
    ordering = ['gene']
    table_class = Phenotypes_MGI_Table
    filter_class = PhenotypeMGIListFilter
    formhelper_class = PhenotypeMGIListFormHelper

    def get_queryset(self):
        qs = super(PhenotypeMGIListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(PhenotypeMGIListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_mgi = Phenotypes_MGI_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_mgi)
        context['table_phenotypes_mgi'] = table_phenotypes_mgi
        return context

class OntologyMGIListView(ExportMixin, PagedFilteredTableView):
    model = Ontology_MGI
    template_name = 'phenotypes_mgi.html'
    context_object_name = 'ontology_mgi'
    ordering = ['mgiphenotype']
    table_class = Ontology_MGI_Table
    filter_class = OntologyMGIListFilter
    formhelper_class = OntologyMGIListFormHelper

    def get_queryset(self):
        qs = super(OntologyMGIListView, self).get_queryset()
        return qs

    def post(self, request, *args, **kwargs):
        return PagedFilteredTableView.as_view()(request)

    def get_context_data(self, **kwargs):
        context = super(OntologyMGIListView, self).get_context_data(**kwargs)
        context['nav_sequences'] = True
        search_query = self.get_queryset()
        table_phenotypes_mgi = Ontology_MGI_Table(search_query)
        RequestConfig(self.request, paginate={'per_page': 20}).configure(table_phenotypes_mgi)
        context['table_phenotypes_mgi'] = table_phenotypes_mgi
        return context

