# filters.py
import django_filters
from .models import Genes,Sequence,Phenotype_SGD,Phenotype_PomBase,Phenotype_TriTrypDB,Phenotype_GenomeCRISPR,Phenotype_WormBase,Phenotype_SeedGenes,Phenotype_ZFIN,Phenotype_ToxoDB,Phenotype_FlyBase,Phenotype_MGI,Alleles_PomBase,Ontology_WormBase,Ontology_MGI

class GenesListFilter(django_filters.FilterSet):
  class Meta:
    model = Genes
    fields = {
            'gene_id': ['icontains',],
            'species': ['exact',],
            'description': ['icontains',],
        }
#    fields = ['gene_id','species','description'] 
#    order_by = ['pk']

class SequenceListFilter(django_filters.FilterSet):
  class Meta:
    model = Sequence
    fields = {
            'species': ['exact',],
            'sequence_id': ['icontains',],
            'peptide_id': ['icontains',],
            'orthomcl_group': ['exact',],
        }

class PhenotypeSGDListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_SGD
    fields = {
            'phenotype': ['icontains',],
            'experiment_type': ['icontains',],
            'mutant_information': ['icontains',],
            'strain': ['icontains',],

        }

class PhenotypePomBaseListFilter(django_filters.FilterSet):
  class Meta:
    model = Alleles_PomBase
    fields = { 
            'FYPO': ['icontains',],
            'allele_description': ['icontains',],
            'allele_type': ['icontains',],
            'evidence': ['icontains',],

        }

class PhenotypeTriTrypDBListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_TriTrypDB
    fields = {
             'phenotype': ['icontains',],
        }

class PhenotypeGenomeCRISPRListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_GenomeCRISPR
    fields = {
             'screen_type': ['icontains',],
             'tested_condition': ['icontains',],
             'cell_line': ['icontains',],
             'log2_fc': ['lte',],
             'ref': ['icontains',],
        }

class PhenotypeWormBaseListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_WormBase
    fields = {
             'modifier': ['exact',],
             'phenotype': ['exact',],
             'details': ['icontains',],
        }

class OntologyWormBaseListFilter(django_filters.FilterSet):
  class Meta:
    model = Ontology_WormBase
    fields = {
             'wbphenotype': ['icontains',],
             'name': ['icontains',],
             'defi': ['icontains',],
        }

class PhenotypeSeedGenesListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_SeedGenes
    fields = {
             'phenotype': ['icontains',],
             'confidence': ['icontains',],
             'ref': ['icontains',],
        }

class PhenotypeZFINListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_ZFIN
    fields = {
             'phenotype_id': ['icontains',],
             'affected_structure': ['icontains',],
             'phenotype_name': ['icontains',],
        }

class PhenotypeToxoDBListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_ToxoDB
    fields = {
             'profile_set': ['icontains',],
             'dataset': ['icontains',],
             'log2_fc': ['lte',],
             'standard_error': ['lte',],
             'fdr': ['lte',],
        }

class PhenotypeFlyBaseListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_FlyBase
    fields = {
             'phenotype': ['icontains',],
        }

class PhenotypeMGIListFilter(django_filters.FilterSet):
  class Meta:
    model = Phenotype_MGI
    fields = {
             'mgi_allele': ['icontains',],
             'allele_type': ['icontains',],
             'allele_attribute': ['icontains',],
             'phenotype_ids': ['icontains',],
        }        

class OntologyMGIListFilter(django_filters.FilterSet):
  class Meta:
    model = Ontology_MGI
    fields = {
             'mgiphenotype': ['icontains',],
             'name': ['icontains',],
             'defi': ['icontains',],
        }


#    fields = ['sequence_id']
#    order_by = ['pk']
