from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.urlresolvers import reverse

class Species(models.Model):
	species_name = models.CharField(max_length=100, default='', primary_key=True)
	tax_id = models.IntegerField(default=0)
	kingdom = models.CharField(max_length=50, default='')

	def __str__(self):
		return self.species_name


class Genes(models.Model):
	gene_id = models.CharField(max_length=50, unique=True, primary_key=True)
	species = models.ForeignKey(Species, on_delete=models.CASCADE)
	description = models.TextField(default='', blank=True)
	
	def __str__(self):
		return self.gene_id


class Sequence(models.Model):
        sequence_id = models.CharField(max_length=50, unique=True, primary_key=True)
        species = models.ForeignKey(Species, on_delete=models.CASCADE)
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        peptide_id = models.CharField(default='',max_length=50,blank=True)
        cds_seq = models.TextField(default='', blank=True)
        peptide_seq= models.TextField(default='', blank=True)
        orthomcl_group = models.CharField(max_length=20, default='', blank=True)

        def __str__(self):
                return self.sequence_id

class Features_external_programs(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_codonw = models.TextField(default='', blank=True)
	features_pepstats = models.TextField(default='', blank=True)
	features_tmhmm = models.TextField(default='', blank=True)
	features_lipop = models.TextField(default='', blank=True)
	features_netcglyc = models.TextField(default='', blank=True)
	features_netnglyc = models.TextField(default='', blank=True)
	features_netoglyc = models.TextField(default='', blank=True)
	features_yinoyang = models.TextField(default='', blank=True)
	features_netchop = models.TextField(default='', blank=True)
	features_netctl = models.TextField(default='', blank=True)
	features_netctlpan = models.TextField(default='', blank=True)
	features_netmhc = models.TextField(default='', blank=True)
	features_netmhc2 = models.TextField(default='', blank=True)
	features_netmhc2pan = models.TextField(default='', blank=True)
	features_netmhcstab = models.TextField(default='', blank=True)
	features_netmhcstabpan = models.TextField(default='', blank=True)
	features_signalp = models.TextField(default='', blank=True)
	features_disembl = models.TextField(default='', blank=True)
	features_wolfpsort = models.TextField(default='', blank=True)

class Features_rdnase(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_tacc = models.TextField(default='', blank=True)
	features_dacc = models.TextField(default='', blank=True)
	features_pseknc = models.TextField(default='', blank=True)
	features_psednc = models.TextField(default='', blank=True)
	features_kmer3 = models.TextField(default='', blank=True)
	features_kmer2 = models.TextField(default='', blank=True)
	features_kmer1 = models.TextField(default='', blank=True)

class Features_protr_Composition(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_aac = models.TextField(default='', blank=True)
	features_dc = models.TextField(default='', blank=True)
	features_tc = models.TextField(default='', blank=True)

class Features_protr_Correlation(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_moreaubroto = models.TextField(default='', blank=True)
	features_geary = models.TextField(default='', blank=True)
	features_moran = models.TextField(default='', blank=True)

class Features_protr_CTDTriad(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_ctdt = models.TextField(default='', blank=True)
	features_ctdd = models.TextField(default='', blank=True)
	features_ctriad = models.TextField(default='', blank=True)
	features_ctdc = models.TextField(default='', blank=True)

class Features_protr_QSO(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_qso = models.TextField(default='', blank=True)
	features_socn = models.TextField(default='', blank=True)

class Features_protr_PseudoComposition(models.Model):
	seq = models.OneToOneField(Sequence, on_delete=models.CASCADE, unique=True)
	features_paac = models.TextField(default='', blank=True)
	features_apaac = models.TextField(default='', blank=True)

class Ontology_WormBase(models.Model):
        wbphenotype = models.CharField(max_length=30,default='',blank=True,unique=True,primary_key=True)
        name = models.TextField(default='', blank=True)
        defi = models.TextField(default='', blank=True)
        is_a = models.TextField(default='', blank=True)

        def __str__(self):
               return self.wbphenotype

class Phenotype_WormBase(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        modifier = models.CharField(max_length=5, default='', blank=True)
        phenotype = models.ForeignKey(Ontology_WormBase, on_delete=models.CASCADE)
        details = models.TextField(default='', blank=True)

class Ontology_MGI(models.Model):
        mgiphenotype = models.CharField(max_length=30,default='',blank=True,unique=True,primary_key=True)
        name = models.TextField(default='', blank=True)
        defi = models.TextField(default='', blank=True)
        is_a = models.TextField(default='', blank=True)

class Phenotype_MGI(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        mgi_allele = models.CharField(max_length=20, default='', blank=True)
        allele_symbol = models.CharField(max_length=200, default='', blank=True)
        allele_type = models.CharField(max_length=300, default='', blank=True)
        allele_attribute = models.CharField(max_length=300, default='', blank=True)
        ref = models.CharField(max_length=100, default='', blank=True)
        mgi_marker = models.CharField(max_length=20, default='', blank=True)
        phenotype_ids = models.TextField(default='', blank=True) 

#class References_FlyBase(models.Model):
#        FBrf = models.CharField(max_length=30, default='')
#        ref = models.CharField(max_length=30, default='', blank=True)
#        complete =  models.TextField(default='', blank=True)

class Phenotype_FlyBase(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE, default='')
        phenotype = models.TextField(default='', blank=True)

class Alleles_PomBase(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        FYPO = models.CharField(max_length=30,default='', blank=True)
        allele_description = models.TextField(default='', blank=True)
        allele_name = models.TextField(default='', blank=True)
        allele_type = models.CharField(max_length=100,default='', blank=True)
        evidence = models.TextField(default='', blank=True)
        ref = models.CharField(max_length=100, default='', blank=True)

class Phenotype_PomBase(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        phenotype = models.TextField(default='', blank=True)

class Phenotype_TriTrypDB(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        phenotype = models.TextField(default='', blank=True)

class Identifiers_ZFIN(models.Model):
        zfin = models.CharField(max_length=100,unique=True,primary_key=True)
        gene = models.ForeignKey(Genes,on_delete=models.CASCADE)

class Phenotype_ZFIN(models.Model):
        zfin = models.ForeignKey(Identifiers_ZFIN, default='', on_delete=models.CASCADE)
        phenotype_id = models.CharField(max_length=30, default='', blank=True)
        affected_structure = models.CharField(max_length=300, default='', blank=True)
        phenotype_name = models.CharField(max_length=300,default='', blank=True)      

class Phenotype_ToxoDB(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        profile_set = models.CharField(max_length=50,default='', blank=True)
        dataset = models.CharField(max_length=50,default='', blank=True)
        log2_fc = models.FloatField(default=0, blank=True)
        standard_error = models.FloatField(default=0, blank=True)
        fdr = models.FloatField(default=0, blank=True)

class Phenotype_SGD(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        phenotype = models.TextField(default='', blank=True)
        experiment_type = models.CharField(max_length=100, default='', blank=True)
        mutant_information = models.CharField(max_length=300, default='', blank=True)
        strain = models.CharField(max_length=200,default='', blank=True)
        ref = models.CharField(max_length=100, default='', blank=True)
      
        def __str__(self):
                return self.gene

class Phenotype_GenomeCRISPR(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        screen_type = models.CharField(max_length=100, default='', blank=True)
        tested_condition = models.CharField(max_length=100, default='', blank=True)
        cell_line = models.CharField(max_length=100, default='', blank=True)
        log2_fc = models.FloatField(default='', blank=True)
        ref = models.CharField(max_length=100, default='', blank=True)

class Phenotype_SeedGenes(models.Model):
        gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
        phenotype = models.CharField(max_length=200, default='', blank=True)
        confidence = models.CharField(max_length=100, default='', blank=True)
        ref = models.TextField(default='', blank=True)

class Phenotypes(models.Model):
	species = models.ForeignKey(Species, on_delete=models.CASCADE)
	gene = models.ForeignKey(Genes, on_delete=models.CASCADE)
	database_source = models.CharField(max_length=30, default='')
	modifier = models.CharField(max_length=5, default='', blank=True)
	phenotype_id = models.CharField(max_length=20, default='', blank=True)
	phenotype_description = models.TextField(default='', blank=True)
	reference = models.TextField(default='', blank=True)

