from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Sequence)
admin.site.register(Species)
