import django_tables2 as tables
from django.views.generic.base import TemplateView
from django_tables2.export.views import ExportMixin
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin,MultiTableMixin
from django_tables2.utils import A
from django_tables2.columns import TemplateColumn
from .models import Species,Sequence,Features_protr_Composition,Features_protr_Correlation,Features_protr_CTDTriad,Features_protr_PseudoComposition,Features_protr_QSO,Features_rdnase,Features_external_programs,Genes,Phenotypes
from .models import Phenotype_SGD,Phenotype_PomBase,Phenotype_TriTrypDB,Phenotype_GenomeCRISPR,Phenotype_WormBase,Phenotype_SeedGenes,Phenotype_ZFIN,Phenotype_ToxoDB,Phenotype_FlyBase,Phenotype_MGI,Alleles_PomBase,Ontology_WormBase,Ontology_MGI
from django.utils.safestring import mark_safe

class DivWrappedColumn(tables.Column):

    def __init__(self, classname=None, *args, **kwargs):
        self.classname=classname
        super(DivWrappedColumn, self).__init__(*args, **kwargs)

    def render(self, value):
        return mark_safe("<div class='" + self.classname + "' >" +value+"</div>")

class SpeciesTable(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Species
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class PhenotypesTable(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotypes
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class SequenceTable(tables.Table):
    export_formats = ['csv']
    gene = tables.Column(accessor='gene_id')
    cds_seq = DivWrappedColumn(classname='cds_seq')
    peptide_seq = DivWrappedColumn(classname='peptide_seq')
    class Meta:
        model = Sequence
        template_name = 'django_tables2/bootstrap-responsive.html'
        attrs = {'width':'50%'}
        ajax = True

class FeaturesProteinsCompositionTable(tables.Table):
    export_formats = ['csv']
    features_aac = DivWrappedColumn(classname='features_aac')
    features_dc = DivWrappedColumn(classname='features_dc')
    features_tc = DivWrappedColumn(classname='features_tc')
    class Meta:
        model = Features_protr_Composition
        fields = ('seq_id','features_aac','features_dc', 'features_tc')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class FeaturesProteinsCorrelationTable(tables.Table):
    export_formats = ['csv']
    features_moreaubroto = DivWrappedColumn(classname='features_moreaubroto')
    features_geary = DivWrappedColumn(classname='features_geary')
    features_moran = DivWrappedColumn(classname='features_moran')
    class Meta:
        model = Features_protr_Correlation
        fields = ('seq_id','features_moreaubroto','features_geary', 'features_moran')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class FeaturesProteinsCTDTriadTable(tables.Table):
    export_formats = ['csv']
    features_ctdt = DivWrappedColumn(classname='features_ctdt')
    features_ctdd = DivWrappedColumn(classname='features_ctdd')
    features_ctriad = DivWrappedColumn(classname='features_ctriad')
    features_ctdc = DivWrappedColumn(classname='features_ctdc')
    class Meta:
        model = Features_protr_CTDTriad
        fields = ('seq_id','features_ctdt','features_ctdd', 'features_ctdc', 'features_ctriad')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class FeaturesProteinsPseudoCompositionTable(tables.Table):
    export_formats = ['csv']
    features_paac = DivWrappedColumn(classname='features_paac')
    features_apaac = DivWrappedColumn(classname='features_apaac')
    class Meta:
        model = Features_protr_PseudoComposition
        fields = ('seq_id','features_paac','features_apaac')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class FeaturesProteinsQSOTable(tables.Table):
    export_formats = ['csv']
    features_qso = DivWrappedColumn(classname='features_qso')
    features_socn = DivWrappedColumn(classname='features_socn')
    class Meta:
        model = Features_protr_QSO
        fields = ('seq_id','features_qso','features_socn')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class FeaturesrDNASeTable(tables.Table):
    export_formats = ['csv']
    features_tacc = DivWrappedColumn(classname='features_tacc')
    features_dacc = DivWrappedColumn(classname='features_dacc')
    features_pseknc = DivWrappedColumn(classname='features_pseknc')
    features_psednc = DivWrappedColumn(classname='features_psednc')
    features_kmer1 = DivWrappedColumn(classname='features_kmer1')
    features_kmer2 = DivWrappedColumn(classname='features_kmer2')
    features_kmer3 = DivWrappedColumn(classname='features_kmer3')
    class Meta:
        model = Features_rdnase
        fields = ('seq_id','features_tacc','features_dacc','features_pseknc','features_psednc','features_kmer1','features_kmer2','features_kmer3')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Features_external_programs_Table(tables.Table):
    export_formats = ['csv']
    features_codonw = DivWrappedColumn(classname='features_codonw')
    features_pepstats = DivWrappedColumn(classname='features_pepstats')
    features_tmhmm = DivWrappedColumn(classname='features_tmhmm')
    features_lipop = DivWrappedColumn(classname='features_lipop')
    features_netcglyc = DivWrappedColumn(classname='features_netcglyc')
    features_netnglyc = DivWrappedColumn(classname='features_netnglyc')
#    features_netoglyc = DivWrappedColumn(classname='features_netoglyc')
    features_yinoyang = DivWrappedColumn(classname='features_yinoyang')
    features_signalp = DivWrappedColumn(classname='features_signalp')
#    features_netoglyc = DivWrappedColumn(classname='features_netoglyc')
#    features_disembl = DivWrappedColumn(classname='features_disembl')
#    features_wolfpsort = DivWrappedColumn(classname='features_wolfpsort')
    class Meta:
        model = Features_external_programs
        fields = ('seq_id','features_codonw','features_pepstats','features_tmhmm','features_lipop','features_netcglyc','features_netnglyc','features_yinoyang','features_signalp')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True


class Features_immuno_programs_Table(tables.Table):
    export_formats = ['csv']
    features_netchop = DivWrappedColumn(classname='features_netchop')
    features_netctl = DivWrappedColumn(classname='features_netctl')
    features_netctlpan = DivWrappedColumn(classname='features_netctlpan')
    features_netmhc = DivWrappedColumn(classname='features_netmhc')
    features_netmhc2 = DivWrappedColumn(classname='features_netmhc2')
    features_netmhc2pan = DivWrappedColumn(classname='features_netmhc2pan')
    features_netmhcstab = DivWrappedColumn(classname='features_netmhcstab')
    features_netmhcstabpan = DivWrappedColumn(classname='features_netmhcstabpan')
    class Meta:
        model = Features_external_programs
        fields = ('seq_id','features_netchop','features_netctl','features_netctlpan','features_netmhc','features_netmhc2','features_netmhc2pan','features_netmhcstab','features_netmhcstabpan')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True


class GenesTable(tables.Table):
    export_formats = ['csv']
    species = tables.Column(accessor='species_id')
    class Meta:
        model = Genes
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class GenesTableSearch(tables.Table):
#     gene_id = tables.LinkColumn('gene-detail', args=[A('pk')])
#     species = tables.LinkColumn('gene-detail', args=[A('pk')])
#     species = tables.Column(accessor='species_id')
     export_formats = ['csv'] 
     class Meta:
        model = Genes
        template_name = 'django_tables2/bootstrap-responsive.html'
        fields = ('gene_id','species','description')
        attrs = {"class": "table-striped table-bordered"}
        empty_text = "There are no genes matching the search criteria..."

class SequenceTableSearch(tables.Table):
     export_formats = ['csv']
     cds_seq = DivWrappedColumn(classname='cds_seq')
     peptide_seq = DivWrappedColumn(classname='peptide_seq')
     class Meta:
        model = Sequence
        template_name = 'django_tables2/bootstrap-responsive.html'
        fields = ('sequence_id')
        attrs = {"class": "table-striped table-bordered"}
        empty_text = "There are no genes matching the search criteria..."


class Phenotypes_WormBase_Table(tables.Table):
    export_formats = ['csv']
    phenotype = tables.Column(accessor='phenotype_id')
    class Meta:
        model = Phenotype_WormBase
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Ontology_MGI_Table(tables.Table):
    export_formats = ['csv']
#    gene = tables.Column(accessor='gene_id')
    class Meta:
        model = Ontology_MGI
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Ontology_WormBase_Table(tables.Table):
    export_formats = ['csv']
#    gene = tables.Column(accessor='gene_id')
    class Meta:
        model = Ontology_WormBase
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_SGD_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_SGD
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_SeedGenes_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_SeedGenes
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_PomBase_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Alleles_PomBase
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_MGI_Table(tables.Table):
    export_formats = ['csv']
    phenotype_ids = DivWrappedColumn(classname='phenotype_ids')
    class Meta:
        model = Phenotype_MGI
        fields = ('gene_id','mgi_allele','allele_type','allele_attribute','phenotype_ids')
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_FlyBase_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_FlyBase
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_ZFIN_Table(tables.Table):
    export_formats = ['csv']
    zfin = tables.Column(accessor='zfin_id')
    class Meta:
        model = Phenotype_ZFIN
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_TriTrypDB_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_TriTrypDB
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_ToxoDB_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_ToxoDB
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True

class Phenotypes_GenomeCRISPR_Table(tables.Table):
    export_formats = ['csv']
    class Meta:
        model = Phenotype_GenomeCRISPR
        template_name = 'django_tables2/bootstrap-responsive.html'
        ajax = True
