from django.conf.urls import url
from blog.models import Post
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
]
