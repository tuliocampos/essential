"""cfehome URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from blog.models import Post
from essential.views import test,home,contact,features_protr_composition,features_protr_correlation,features_protr_qso,features_protr_ctdtriad,features_protr_pseudocomposition,features_rdnase,features_external_programs,features_immuno_programs,predictor,species,phenotypes,essentiality,GenesListView,SequenceListView
from essential.views import PhenotypeSGDListView,PhenotypePomBaseListView,PhenotypeTriTrypDBListView,PhenotypeGenomeCRISPRListView,PhenotypeWormBaseListView,PhenotypeSeedGenesListView,PhenotypeZFINListView,PhenotypeToxoDBListView,PhenotypeFlyBaseListView,PhenotypeMGIListView,OntologyWormBaseListView,OntologyMGIListView

from blog.views import post_list

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home),
    url(r'^species/', species),
    url(r'^test/', test),
    url(r'^features_protr_correlation', features_protr_correlation),
    url(r'^features_protr_qso', features_protr_qso),
    url(r'^features_protr_ctdtriad', features_protr_ctdtriad),
    url(r'^features_protr_pseudocomposition', features_protr_pseudocomposition),
    url(r'^features_rdnase', features_rdnase),
    url(r'^features_external_programs', features_external_programs),
    url(r'^features_immuno_programs', features_immuno_programs),
    url(r'^features', features_protr_composition),
    url(r'^sequences/', SequenceListView.as_view()),
    url(r'^genes/', GenesListView.as_view()),
    url(r'^phenotypes/', phenotypes),
    url(r'^contact/', contact),
    url(r'^predictor/', predictor),
    url(r'^blog/', post_list),
    url(r'^essentiality/', essentiality),
    url(r'^sgd/', PhenotypeSGDListView.as_view()),
    url(r'^pombase/', PhenotypePomBaseListView.as_view()),
    url(r'^tritrypdb/', PhenotypeTriTrypDBListView.as_view()),
    url(r'^genomecrispr/', PhenotypeGenomeCRISPRListView.as_view()),
    url(r'^wormbase/', PhenotypeWormBaseListView.as_view()),
    url(r'^ontology_wormbase/', OntologyWormBaseListView.as_view()),
    url(r'^ontology_mgi/', OntologyMGIListView.as_view()),
    url(r'^seedgenes/', PhenotypeSeedGenesListView.as_view()),
    url(r'^zfin/', PhenotypeZFINListView.as_view()),
    url(r'^toxodb/', PhenotypeToxoDBListView.as_view()),
    url(r'^flybase/', PhenotypeFlyBaseListView.as_view()),
    url(r'^mgi/', PhenotypeMGIListView.as_view()),
#   url(r'', include('blog.urls')), 
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

