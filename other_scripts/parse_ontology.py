#!/usr/bin/env python
import os
import sys

ide=''
name=''
defi=''
f=open(sys.argv[1],'r')
for line in f:
    if line.startswith( '[Term]'):
      if ide!='':
       print (ide+"\t"+name+"\t"+defi)
      ide=''
      name=''
      defi=''
    if line.startswith( 'id:'):
     ide=ide+line.rstrip('\r\n')[4:]
    if line.startswith( 'name:'):
     name=name+line.rstrip('\r\n')[6:]
    if line.startswith( 'def:'):
     defi=defi+line.rstrip('\r\n')[5:]
if ide!='':
       print (ide+"\t"+name+"\t"+defi)
