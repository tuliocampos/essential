/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R C_elegans
tail -1000 logfile_FS.txt > logfile_CE2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R D_melanogaster
tail -1000 logfile_FS.txt > logfile_DM2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R M_musculus
tail -1000 logfile_FS.txt > logfile_MM2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R S_cerevisiae
tail -1000 logfile_FS.txt > logfile_SC2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R S_pombe
tail -1000 logfile_FS.txt > logfile_SP2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R H_sapiens
tail -1000 logfile_FS.txt > logfile_HS2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R M_musculus_KABIR
tail -1000 logfile_FS.txt > logfile_Kabir2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R H_sapiens_GUO
tail -1000 logfile_FS.txt > logfile_Guo2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_C_elegans
tail -1000 logfile_FS.txt > logfile_ALL_CE2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_D_melanogaster
tail -1000 logfile_FS.txt > logfile_ALL_DM2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_H_sapiens
tail -1000 logfile_FS.txt > logfile_ALL_HS2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_M_musculus
tail -1000 logfile_FS.txt > logfile_ALL_MM2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_S_cerevisiae
tail -1000 logfile_FS.txt > logfile_ALL_SC2.txt
/usr/local/bin/Rscript Caret_FS_REAL_OGEE.R ALLBUT_S_pombe
tail -1000 logfile_FS.txt > logfile_ALL_SP2.txt
