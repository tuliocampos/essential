#!/usr/bin/env python
import os
import sys

p=open(sys.argv[1],'r')
#p = open(tmp,"r")
result = {'TMD':0,'EXPAA':0,'EXP60':0,'PROBN':0}
lines = p.readlines()
p.close()

result['TMD']     = lines[1].split()[6]
result['EXPAA'] = lines[2].split()[8]
result['EXP60']    = lines[3].split()[7]
result['PROBN']    = lines[4].split()[6]

#print (dict.items(result))
#print (dict.keys(result),dict.keys(DayhoffStat))
print (dict.values(result))

