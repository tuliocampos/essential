/usr/local/bin/Rscript Caret_REAL_OGEE.R C_elegans
tail -1000 logfile_Caret.txt > logfile_CE.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R D_melanogaster
tail -1000 logfile_Caret.txt > logfile_DM.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R M_musculus
tail -1000 logfile_Caret.txt > logfile_MM.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R S_cerevisiae
tail -1000 logfile_Caret.txt > logfile_SC.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R S_pombe
tail -1000 logfile_Caret.txt > logfile_SP.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R H_sapiens
tail -1000 logfile_Caret.txt > logfile_HS.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R M_musculus_KABIR
tail -1000 logfile_Caret.txt > logfile_Kabir.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R H_sapiens_GUO
tail -1000 logfile_Caret.txt > logfile_Guo.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_C_elegans
tail -1000 logfile_Caret.txt > logfile_ALL_CE.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_D_melanogaster
tail -1000 logfile_Caret.txt > logfile_ALL_DM.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_H_sapiens
tail -1000 logfile_Caret.txt > logfile_ALL_HS.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_M_musculus
tail -1000 logfile_Caret.txt > logfile_ALL_MM.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_S_cerevisiae
tail -1000 logfile_Caret.txt > logfile_ALL_SC.txt
/usr/local/bin/Rscript Caret_REAL_OGEE.R ALLBUT_S_pombe
tail -1000 logfile_Caret.txt > logfile_ALL_SP.txt
