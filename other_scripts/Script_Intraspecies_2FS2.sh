nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_C_elegans > logfile_ALL_CE2.txt &
nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_D_melanogaster > logfile_ALL_DM2.txt &
nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_H_sapiens > logfile_ALL_HS2.txt &
nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_M_musculus > logfile_ALL_MM2.txt &
nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_S_cerevisiae > logfile_ALL_SC2.txt &
nohup /usr/local/bin/Rscript Caret_ALL_FS_REAL_OGEE.R ALLBUT_S_pombe > logfile_ALL_SP2.txt &
