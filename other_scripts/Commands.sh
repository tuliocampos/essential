wget http://ogee.medgenius.info/file_download/genes.txt.gz
wget http://ogee.medgenius.info/file_download/connectivity.txt.gz
wget http://ogee.medgenius.info/file_download/phyleticage.txt.gz
wget http://ogee.medgenius.info/file_download/datasets.txt.gz
wget http://ogee.medgenius.info/file_download/gene_essentiality.txt.gz
gunzip *.gz
#cat gene_essentiality.txt | grep "eukaryota" | awk -F "\t" '{if($5=="E")print $0}' | sort | uniq > Essential_list.txt
#cat gene_essentiality.txt | grep "eukaryota" | awk -F "\t" '{if($5=="NE")print $0}' | sort | uniq > NEssential_list.txt
#cat Essential_list.txt | sort | uniq | awk '{print $5}' | sort | uniq > Essential_list_IDs.txt
#cat NEssential_list.txt | sort | uniq | awk '{print $5}' | sort | uniq > NEssential_list_IDs.txt
#for i in `cat Essential_list_IDs.txt`; do grep ${i} genes.txt >> Essential_genes.txt; done
#for i in `cat NEssential_list_IDs.txt`; do grep ${i} genes.txt >> NEssential_genes.txt; done
cat genes.txt | awk -F "\t" '{if($1=="Danio rerio")print $0}' > genes_DR.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "Danio" |  awk -F "\t" '{if($5=="E")print $0}' > D_rerio_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "Danio" |  awk -F "\t" '{if($5=="NE")print $0}' > D_rerio_NEssential_list.txt
cat D_rerio_Essential_list.txt | awk '{print $5}' | sort | uniq > D_rerio_Essential_list_IDs.txt
cat D_rerio_NEssential_list.txt | awk '{print $5}' | sort | uniq > D_rerio_NEssential_list_IDs.txt
grep -w -f D_rerio_NEssential_list_IDs.txt D_rerio_Essential_list_IDs.txt > D_rerio_divergent.txt
grep -w -v -f D_rerio_divergent.txt D_rerio_Essential_list_IDs.txt > D_rerio_Essential_list_IDs_clean.txt
grep -w -v -f D_rerio_divergent.txt D_rerio_NEssential_list_IDs.txt > D_rerio_NEssential_list_IDs_clean.txt
for i in `cat D_rerio_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_DR.txt | head -1 >> D_rerio_Essential_genes.txt; done
for i in `cat D_rerio_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_DR.txt |head -1 >> D_rerio_NEssential_genes.txt; done
cat D_rerio_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > D_rerio_Essential_PROTEINS.txt
cat D_rerio_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > D_rerio_NEssential_PROTEINS.txt
cat D_rerio_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > D_rerio_Essential_CDS.txt
cat D_rerio_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > D_rerio_NEssential_CDS.txt
cat genes.txt | awk -F "\t" '{if($1=="Caenorhabditis elegans")print $0}' > genes_CE.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "Caenorhabditis" |  awk -F "\t" '{if($5=="E")print $0}' > C_elegans_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "Caenorhabditis" |  awk -F "\t" '{if($5=="NE")print $0}' > C_elegans_NEssential_list.txt
cat C_elegans_Essential_list.txt | awk '{print $5}' | sort | uniq > C_elegans_Essential_list_IDs.txt
cat C_elegans_NEssential_list.txt | awk '{print $5}' | sort | uniq > C_elegans_NEssential_list_IDs.txt
grep -w -f C_elegans_NEssential_list_IDs.txt C_elegans_Essential_list_IDs.txt > C_elegans_divergent.txt
grep -w -v -f C_elegans_divergent.txt C_elegans_Essential_list_IDs.txt > C_elegans_Essential_list_IDs_clean.txt
grep -w -v -f C_elegans_divergent.txt C_elegans_NEssential_list_IDs.txt > C_elegans_NEssential_list_IDs_clean.txt
for i in `cat C_elegans_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_CE.txt | head -1 >> C_elegans_Essential_genes.txt; done
for i in `cat C_elegans_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_CE.txt |head -1 >> C_elegans_NEssential_genes.txt; done
cat C_elegans_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > C_elegans_Essential_PROTEINS.txt
cat C_elegans_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > C_elegans_NEssential_PROTEINS.txt
cat C_elegans_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > C_elegans_Essential_CDS.txt
cat C_elegans_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > C_elegans_NEssential_CDS.txt
#cat NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > NEssential_CDS.txt
#cat Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > Essential_CDS.txt
#cat Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > Essential_PROTEINS.txt
#cat NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > NEssential_PROTEINS.txt
cat genes.txt | awk -F "\t" '{if($1=="Schizosaccharomyces pombe")print $0}' > genes_SP.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "pombe" |  awk -F "\t" '{if($5=="E")print $0}' > S_pombe_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "pombe" |  awk -F "\t" '{if($5=="NE")print $0}' > S_pombe_NEssential_list.txt
cat S_pombe_Essential_list.txt | awk '{print $5}' | sort | uniq > S_pombe_Essential_list_IDs.txt
cat S_pombe_NEssential_list.txt | awk '{print $5}' | sort | uniq > S_pombe_NEssential_list_IDs.txt
grep -w -f S_pombe_NEssential_list_IDs.txt S_pombe_Essential_list_IDs.txt > S_pombe_divergent.txt
grep -w -v -f S_pombe_divergent.txt S_pombe_Essential_list_IDs.txt > S_pombe_Essential_list_IDs_clean.txt
grep -w -v -f S_pombe_divergent.txt S_pombe_NEssential_list_IDs.txt > S_pombe_NEssential_list_IDs_clean.txt
for i in `cat S_pombe_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_SP.txt | head -1 >> S_pombe_Essential_genes.txt; done
for i in `cat S_pombe_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_SP.txt | head -1 >> S_pombe_NEssential_genes.txt; done
cat S_pombe_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > S_pombe_Essential_PROTEINS.txt
cat S_pombe_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > S_pombe_NEssential_PROTEINS.txt
cat S_pombe_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > S_pombe_Essential_CDS.txt
cat S_pombe_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > S_pombe_NEssential_CDS.txt
cat genes.txt | awk -F "\t" '{if($1=="Saccharomyces cerevisiae")print $0}' > genes_SC.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "cerevisiae" |  awk -F "\t" '{if($5=="E")print $0}' > S_cerevisiae_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "cerevisiae" |  awk -F "\t" '{if($5=="NE")print $0}' > S_cerevisiae_NEssential_list.txt
cat S_cerevisiae_Essential_list.txt | awk '{print $5}' | sort | uniq > S_cerevisiae_Essential_list_IDs.txt
cat S_cerevisiae_NEssential_list.txt | awk '{print $5}' | sort | uniq > S_cerevisiae_NEssential_list_IDs.txt
grep -w -f S_cerevisiae_NEssential_list_IDs.txt S_cerevisiae_Essential_list_IDs.txt > S_cerevisiae_divergent.txt
grep -w -v -f S_cerevisiae_divergent.txt S_cerevisiae_Essential_list_IDs.txt > S_cerevisiae_Essential_list_IDs_clean.txt
grep -w -v -f S_cerevisiae_divergent.txt S_cerevisiae_NEssential_list_IDs.txt > S_cerevisiae_NEssential_list_IDs_clean.txt
for i in `cat S_cerevisiae_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_SC.txt | head -1 >> S_cerevisiae_Essential_genes.txt; done
for i in `cat S_cerevisiae_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_SC.txt | head -1 >> S_cerevisiae_NEssential_genes.txt; done
cat S_cerevisiae_Essential_genes.txt | sort | uniq | awk -F "\t" '{print ">"$4"\n"$7}' | perl -pi -e 's/\*//g' > S_cerevisiae_Essential_PROTEINS.txt
cat S_cerevisiae_NEssential_genes.txt | sort | uniq | awk -F "\t" '{print ">"$4"\n"$7}' | perl -pi -e 's/\*//g' > S_cerevisiae_NEssential_PROTEINS.txt
cat S_cerevisiae_Essential_genes.txt | sort | uniq | awk -F "\t" '{print ">"$4"\n"$8}' | perl -pi -e 's/\*//g' > S_cerevisiae_Essential_CDS.txt
cat S_cerevisiae_NEssential_genes.txt | sort | uniq | awk -F "\t" '{print ">"$4"\n"$8}' | perl -pi -e 's/\*//g' > S_cerevisiae_NEssential_CDS.txt
cat genes.txt | awk -F "\t" '{if($1=="Mus musculus")print $0}' > genes_MM.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "musculus" |  awk -F "\t" '{if($5=="E")print $0}' > M_musculus_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "musculus" |  awk -F "\t" '{if($5=="NE")print $0}' > M_musculus_NEssential_list.txt
cat M_musculus_Essential_list.txt | awk '{print $5}' | sort | uniq > M_musculus_Essential_list_IDs.txt
cat M_musculus_NEssential_list.txt | awk '{print $5}' | sort | uniq > M_musculus_NEssential_list_IDs.txt
grep -w -f M_musculus_NEssential_list_IDs.txt M_musculus_Essential_list_IDs.txt > M_musculus_divergent.txt
grep -w -v -f M_musculus_divergent.txt M_musculus_Essential_list_IDs.txt > M_musculus_Essential_list_IDs_clean.txt
grep -w -v -f M_musculus_divergent.txt M_musculus_NEssential_list_IDs.txt > M_musculus_NEssential_list_IDs_clean.txt
for i in `cat M_musculus_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_MM.txt | head -1 >> M_musculus_Essential_genes.txt; done
for i in `cat M_musculus_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_MM.txt | head -1 >> M_musculus_NEssential_genes.txt; done
cat M_musculus_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > M_musculus_Essential_PROTEINS.txt
cat M_musculus_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > M_musculus_NEssential_PROTEINS.txt
cat M_musculus_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > M_musculus_Essential_CDS.txt
cat M_musculus_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > M_musculus_NEssential_CDS.txt
cat genes.txt | awk -F "\t" '{if($1=="Drosophila melanogaster")print $0}' > genes_DM.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "melanogaster" |  awk -F "\t" '{if($5=="E")print $0}' > D_melanogaster_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "melanogaster" |  awk -F "\t" '{if($5=="NE")print $0}' > D_melanogaster_NEssential_list.txt
cat D_melanogaster_Essential_list.txt | awk '{print $5}' | sort | uniq > D_melanogaster_Essential_list_IDs.txt
cat D_melanogaster_NEssential_list.txt | awk '{print $5}' | sort | uniq > D_melanogaster_NEssential_list_IDs.txt
grep -w -f D_melanogaster_NEssential_list_IDs.txt D_melanogaster_Essential_list_IDs.txt > D_melanogaster_divergent.txt
grep -w -v -f D_melanogaster_divergent.txt D_melanogaster_Essential_list_IDs.txt > D_melanogaster_Essential_list_IDs_clean.txt
grep -w -v -f D_melanogaster_divergent.txt D_melanogaster_NEssential_list_IDs.txt > D_melanogaster_NEssential_list_IDs_clean.txt
for i in `cat D_melanogaster_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_DM.txt | head -1 >> D_melanogaster_Essential_genes.txt; done
for i in `cat D_melanogaster_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_DM.txt | head -1 >>  D_melanogaster_NEssential_genes.txt; done
cat D_melanogaster_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > D_melanogaster_Essential_PROTEINS.txt
cat D_melanogaster_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > D_melanogaster_NEssential_PROTEINS.txt
cat D_melanogaster_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > D_melanogaster_Essential_CDS.txt
cat D_melanogaster_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > D_melanogaster_NEssential_CDS.txt
cat genes.txt | awk -F "\t" '{if($1=="Homo sapiens")print $0}' > genes_HS.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "sapiens" |  awk -F "\t" '{if($5=="E")print $0}' > H_sapiens_Essential_list.txt
cat gene_essentiality.txt | grep "eukaryota" | grep "sapiens" |  awk -F "\t" '{if($5=="NE")print $0}' > H_sapiens_NEssential_list.txt
cat H_sapiens_Essential_list.txt | awk '{print $5}' | sort | uniq > H_sapiens_Essential_list_IDs.txt
cat H_sapiens_NEssential_list.txt | awk '{print $5}' | sort | uniq > H_sapiens_NEssential_list_IDs.txt
grep -w -f H_sapiens_NEssential_list_IDs.txt H_sapiens_Essential_list_IDs.txt > H_sapiens_divergent.txt
grep -w -v -f H_sapiens_divergent.txt H_sapiens_Essential_list_IDs.txt > H_sapiens_Essential_list_IDs_clean.txt
grep -w -v -f H_sapiens_divergent.txt H_sapiens_NEssential_list_IDs.txt > H_sapiens_NEssential_list_IDs_clean.txt
for i in `cat H_sapiens_Essential_list_IDs_clean.txt`; do grep -w ${i} genes_HS.txt | head -1 >> H_sapiens_Essential_genes.txt; done
for i in `cat H_sapiens_NEssential_list_IDs_clean.txt`; do grep -w ${i} genes_HS.txt | head -1 >> H_sapiens_NEssential_genes.txt; done
cat H_sapiens_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > H_sapiens_Essential_PROTEINS.txt
cat H_sapiens_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$7}' > H_sapiens_NEssential_PROTEINS.txt
cat H_sapiens_Essential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > H_sapiens_Essential_CDS.txt
cat H_sapiens_NEssential_genes.txt | awk -F "\t" '{print ">"$4"\n"$8}' > H_sapiens_NEssential_CDS.txt
cat D_melanogaster_NEssential_PROTEINS.txt H_sapiens_GUO_NEssential_PROTEINS.txt M_musculus_KABIR_NEssential_PROTEINS.txt S_cerevisiae_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_C_elegans_NEssential_PROTEINS.txt
cat D_melanogaster_Essential_PROTEINS.txt H_sapiens_GUO_Essential_PROTEINS.txt M_musculus_KABIR_Essential_PROTEINS.txt S_cerevisiae_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_C_elegans_Essential_PROTEINS.txt
cat C_elegans_NEssential_PROTEINS.txt H_sapiens_GUO_NEssential_PROTEINS.txt M_musculus_KABIR_NEssential_PROTEINS.txt S_cerevisiae_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_D_melanogaster_NEssential_PROTEINS.txt
cat C_elegans_Essential_PROTEINS.txt H_sapiens_GUO_Essential_PROTEINS.txt M_musculus_KABIR_Essential_PROTEINS.txt S_cerevisiae_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_D_melanogaster_Essential_PROTEINS.txt
cat C_elegans_NEssential_PROTEINS.txt D_melanogaster_NEssential_PROTEINS.txt M_musculus_KABIR_NEssential_PROTEINS.txt S_cerevisiae_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_H_sapiens_NEssential_PROTEINS.txt
cat C_elegans_Essential_PROTEINS.txt D_melanogaster_Essential_PROTEINS.txt M_musculus_KABIR_Essential_PROTEINS.txt S_cerevisiae_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_H_sapiens_Essential_PROTEINS.txt
cat C_elegans_NEssential_PROTEINS.txt D_melanogaster_NEssential_PROTEINS.txt H_sapiens_GUO_NEssential_PROTEINS.txt S_cerevisiae_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_M_musculus_NEssential_PROTEINS.txt
cat C_elegans_Essential_PROTEINS.txt D_melanogaster_Essential_PROTEINS.txt H_sapiens_GUO_Essential_PROTEINS.txt S_cerevisiae_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_M_musculus_Essential_PROTEINS.txt
cat C_elegans_NEssential_PROTEINS.txt D_melanogaster_NEssential_PROTEINS.txt H_sapiens_GUO_NEssential_PROTEINS.txt M_musculus_KABIR_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_S_cerevisiae_NEssential_PROTEINS.txt
cat C_elegans_Essential_PROTEINS.txt D_melanogaster_Essential_PROTEINS.txt H_sapiens_GUO_Essential_PROTEINS.txt M_musculus_KABIR_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_S_cerevisiae_Essential_PROTEINS.txt
cat C_elegans_NEssential_PROTEINS.txt D_melanogaster_NEssential_PROTEINS.txt H_sapiens_GUO_NEssential_PROTEINS.txt M_musculus_KABIR_NEssential_PROTEINS.txt S_cerevisiae_NEssential_PROTEINS.txt S_pombe_NEssential_PROTEINS.txt > ALLBUT_S_pombe_NEssential_PROTEINS.txt
cat C_elegans_Essential_PROTEINS.txt D_melanogaster_Essential_PROTEINS.txt H_sapiens_GUO_Essential_PROTEINS.txt M_musculus_KABIR_Essential_PROTEINS.txt S_cerevisiae_Essential_PROTEINS.txt S_pombe_Essential_PROTEINS.txt > ALLBUT_S_pombe_Essential_PROTEINS.txt
