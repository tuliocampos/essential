library(biomaRt)
#listAttributes(ensembl)
#data_HS <- getBM(attributes=c('ensembl_gene_id','ensembl_transcript_id', 'description', 'strand', 'transcript_count', 'percentage_gene_gc_content', 'go_id', 'kegg_enzyme',
#                              'pfam', 'interpro', 'interpro_short_description', 'tmhmm', 'signalp', 'ncoils' ), mart = ensembl)

ensembl = useEnsembl(biomart="ensembl", dataset="mmusculus_gene_ensembl")
data_MM <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useEnsembl(biomart="ensembl", dataset="celegans_gene_ensembl")
data_CE <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useEnsembl(biomart="ensembl", dataset="dmelanogaster_gene_ensembl")
data_DM <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useEnsembl(biomart="ensembl", dataset="hsapiens_gene_ensembl")
data_HS <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useEnsembl(biomart="ensembl", dataset="drerio_gene_ensembl")
data_DR <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useMart(biomart = "fungi_mart", host = "fungi.ensembl.org")
#listDatasets(ensembl)
ensembl <- useDataset(mart = ensembl, dataset = "scerevisiae_eg_gene")

data_SC <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useMart(biomart = "fungi_mart", host = "fungi.ensembl.org")
#listDatasets(ensembl)
ensembl <- useDataset(mart = ensembl, dataset = "spombe_eg_gene")

data_SP <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)


ensembl = useMart(biomart = "fungi_mart", host = "fungi.ensembl.org")
#listDatasets(ensembl)
ensembl <- useDataset(mart = ensembl, dataset = "afumigatus_eg_gene")

data_AF <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useMart(biomart = "protists_mart", host = "protists.ensembl.org")

ensembl <- useDataset(mart = ensembl, dataset = "tgondii_eg_gene")

data_TG <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useMart(biomart = "protists_mart", host = "protists.ensembl.org")

ensembl <- useDataset(mart = ensembl, dataset = "tbrucei_eg_gene")

data_TB <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

ensembl = useMart(biomart = "plants_mart", host = "plants.ensembl.org")

ensembl <- useDataset(mart = ensembl, dataset = "athaliana_eg_gene")

data_AT <- getBM(attributes=c('ensembl_gene_id', 'description', 'percentage_gene_gc_content'), mart=ensembl)

