#install.packages("e1071")
#install.packages("protr")
#install.packages("randomForest")
#install.packages("pROC")
#install.packages("neuralnet")
#install.packages("gbm")
#install.packages("nnet")
#install.packages("ggplot2")


#setwd("/home/tulio/Essential_data")

setwd("/home/tulio/Essential_data/Guo_data")

library("e1071")
library("protr")
library("randomForest")
library("pROC")
library("neuralnet")
library("gbm")
#library("corrplot")

#essential = readFASTA("C_elegans_Essential_PROTEINS.txt")
#nessential = readFASTA("C_elegans_NEssential_PROTEINS.txt")

essential = readFASTA("Guo_Essential_PROTEINS.fasta")
nessential = readFASTA("Guo_Essential_PROTEINS.fasta")

essential = essential[(sapply(essential, protcheck))]
nessential = nessential[(sapply(nessential, protcheck))]

a1 = t(sapply(essential, extractAAC))
a2 = t(sapply(nessential, extractAAC))
a3 = t(sapply(essential, extractDC))
a4 = t(sapply(nessential, extractDC))

a5 = t(sapply(essential, extractTC))
a6 = t(sapply(nessential, extractTC))

x1=cbind(a1,a3)
x2=cbind(a2,a4)

x = rbind(cbind(x1,a5),cbind(x2,a6))

colnames(x) <- paste("Amino", colnames(x), sep = "_")

labels = as.factor(c(rep(1, length(essential)), rep(0, length(nessential))))

#https://cran.r-project.org/web/packages/corrplot/vignettes/corrplot-intro.html
#corrplot(cor(x), diag = FALSE, order = "FPC", tl.pos = "td", tl.cex = 0.5, method = "color", type = "upper")

labels_ = c(rep(1, length(essential)), rep(0, length(nessential)))

Cessential = readFASTA("C_elegans_Essential_PROTEINS.txt")
Cnessential = readFASTA("C_elegans_NEssential_PROTEINS.txt")

Cessential = Cessential[(sapply(Cessential, protcheck))]
Cnessential = Cnessential[(sapply(Cnessential, protcheck))]

a1 = t(sapply(Cessential, extractAAC))
a2 = t(sapply(Cnessential, extractAAC))
a3 = t(sapply(Cessential, extractDC))
a4 = t(sapply(Cnessential, extractDC))

a5 = t(sapply(Cessential, extractTC))
a6 = t(sapply(Cnessential, extractTC))

c1=cbind(a1,a3)
c2=cbind(a2,a4)

c = rbind(cbind(c1,a5),cbind(c2,a6))

colnames(c) <- paste("Amino", colnames(c), sep = "_")

labels_test_c = as.factor(c(rep(1, length(Cessential)), rep(0, length(Cnessential))))

Dessential = readFASTA("D_melanogaster_Essential_PROTEINS.txt")
Dnessential = readFASTA("D_melanogaster_NEssential_PROTEINS.txt")

Dessential = Dessential[(sapply(Dessential, protcheck))]
Dnessential = Dnessential[(sapply(Dnessential, protcheck))]

a1 = t(sapply(Dessential, extractAAC))
a2 = t(sapply(Dnessential, extractAAC))
a3 = t(sapply(Dessential, extractDC))
a4 = t(sapply(Dnessential, extractDC))

a5 = t(sapply(Dessential, extractTC))
a6 = t(sapply(Dnessential, extractTC))

d1=cbind(a1,a3)
d2=cbind(a2,a4)

d = rbind(cbind(d1,a5),cbind(d2,a6))

colnames(d) <- paste("Amino", colnames(d), sep = "_")

labels_test_d = as.factor(c(rep(1, length(Dessential)), rep(0, length(Dnessential))))

Messential = readFASTA("M_musculus_Essential_PROTEINS.txt")
Mnessential = readFASTA("M_musculus_NEssential_PROTEINS.txt")

Messential = Messential[(sapply(Messential, protcheck))]
Mnessential = Mnessential[(sapply(Mnessential, protcheck))]

a1 = t(sapply(Messential, extractAAC))
a2 = t(sapply(Mnessential, extractAAC))
a3 = t(sapply(Messential, extractDC))
a4 = t(sapply(Mnessential, extractDC))

a5 = t(sapply(Messential, extractTC))
a6 = t(sapply(Mnessential, extractTC))

m1=cbind(a1,a3)
m2=cbind(a2,a4)

m = rbind(cbind(m1,a5),cbind(m2,a6))

colnames(m) <- paste("Amino", colnames(m), sep = "_")

labels_test_m = as.factor(c(rep(1, length(Messential)), rep(0, length(Mnessential))))

Pessential = readFASTA("S_pombe_Essential_PROTEINS.txt")
Pnessential = readFASTA("S_pombe_NEssential_PROTEINS.txt")

Pessential = Pessential[(sapply(Pessential, protcheck))]
Pnessential = Pnessential[(sapply(Pnessential, protcheck))]

a1 = t(sapply(Pessential, extractAAC))
a2 = t(sapply(Pnessential, extractAAC))
a3 = t(sapply(Pessential, extractDC))
a4 = t(sapply(Pnessential, extractDC))

a5 = t(sapply(Pessential, extractTC))
a6 = t(sapply(Pnessential, extractTC))

p1=cbind(a1,a3)
p2=cbind(a2,a4)

p = rbind(cbind(p1,a5),cbind(p2,a6))

colnames(p) <- paste("Amino", colnames(p), sep = "_")

labels_test_p = as.factor(c(rep(1, length(Pessential)), rep(0, length(Pnessential))))


Sessential = readFASTA("S_cerevisiae_Essential_PROTEINS.txt")
Snessential = readFASTA("S_cerevisiae_NEssential_PROTEINS.txt")

Sessential = Sessential[(sapply(Sessential, protcheck))]
Snessential = Snessential[(sapply(Snessential, protcheck))]

a1 = t(sapply(Sessential, extractAAC))
a2 = t(sapply(Snessential, extractAAC))
a3 = t(sapply(Sessential, extractDC))
a4 = t(sapply(Snessential, extractDC))

a5 = t(sapply(Sessential, extractTC))
a6 = t(sapply(Snessential, extractTC))

s1=cbind(a1,a3)
s2=cbind(a2,a4)

s = rbind(cbind(s1,a5),cbind(s2,a6))

colnames(s) <- paste("Amino", colnames(s), sep = "_")

labels_test_s = as.factor(c(rep(1, length(Sessential)), rep(0, length(Snessential))))

cycles=9
step=round(length(essential)/10)

set.seed(99)

total_rfp <- vector(mode="numeric", length=cycles)
total_rfc <- vector(mode="numeric", length=cycles)
total_rfd <- vector(mode="numeric", length=cycles)
total_rfm <- vector(mode="numeric", length=cycles)
total_rfs <- vector(mode="numeric", length=cycles)
total_gbm_p <- vector(mode="numeric", length=cycles)
total_gbm_c <- vector(mode="numeric", length=cycles)
total_gbm_d <- vector(mode="numeric", length=cycles)
total_gbm_m <- vector(mode="numeric", length=cycles)
total_gbm_s <- vector(mode="numeric", length=cycles)
total_svm_p <- vector(mode="numeric", length=cycles)
total_svm_c <- vector(mode="numeric", length=cycles)
total_svm_d <- vector(mode="numeric", length=cycles)
total_svm_m <- vector(mode="numeric", length=cycles)
total_svm_s <- vector(mode="numeric", length=cycles)

test_rf <- vector(mode="numeric", length=cycles)
test_gbm <- vector(mode="numeric", length=cycles)
test_svm <- vector(mode="numeric", length=cycles)
test_nn <- vector(mode="numeric", length=cycles)
test_lm <- vector(mode="numeric", length=cycles)

for(i in seq(step, 9*step, by = step)){
  
  tr.idx = c(sample(1:nrow(x1), i),
             sample(nrow(x1)+1:nrow(x2), i)) 
  te.idx = setdiff(1:nrow(x), tr.idx)
  x.tr = x[tr.idx, ]
  y.tr = labels[tr.idx]
  x.te = x[te.idx, ]
  y.te = labels[te.idx]
  
##Prep NN
  
  labels_ = c(rep(1, length(essential)), rep(0, length(nessential)))
  
  x_data=as.data.frame(cbind(x,labels_))
  
  train_ = x_data[tr.idx,]
  test_ = x_data[te.idx,]

##NN train
  
  n = names(train_)
  f = as.formula(paste("labels_ ~", paste(n[!n %in% "labels_"], collapse = " + ")))
  nn = neuralnet(f,data=train_,hidden=1,linear.output=T)
  
#  pr.nn = compute(nn,test_[,1:dim(x1)[2]])
  
#  pr.nn_ = pr.nn$net.result*(max(x_data$labels)-min(x_data$labels))+min(x_data$labels)
#  test.r = (test_$labels)*(max(x_data$labels)-min(x_data$labels))+min(x_data$labels)
  
#  temp=roc(as.numeric(test.r), as.numeric(pr.nn_))
  
#  total_nn[i/step] = as.numeric(temp$auc)

##LM train
      
  train.lm = as.data.frame(train_)
  
  test.lm = as.data.frame(test_)
  
  lm.fit = glm(labels_~., data=train.lm)
  
#  pr.lm = predict(lm.fit,test.lm,type = "response")
  
#  temp=roc(as.numeric(test.lm$labels_), as.numeric(pr.lm))
  
#  total_lm[i/step] = as.numeric(temp$auc)
  
###
  
  rf.fit = tuneRF(x.tr, y.tr, doBest=TRUE)
  
  #  tune.svm(...)$best.model
  # 
  #  obj <- tune.svm(Species~., data = iris, sampling = "fix", gamma = 2^c(-8,-4,0,4), cost = 2^c(-8,-4,-2,0))
  
  #svm.fit = svm(x.tr,y.tr, kernel="linear", type="C-classification", probability = TRUE)
  
  svm.fit = tune.svm(x.tr,y.tr, kernel="radial", type="C-classification", probability = TRUE, gamma = 2^c(-8,-4,0,4), cost = 2^c(-8,-4,-2,0))$best.model
  
  svm.pred = predict(svm.fit, c, probability=TRUE, decision.values = TRUE)
  
  temp=roc(labels_test_c, attributes(svm.pred)$probabilities[,1])
  
  total_svm_c[i/step] = as.numeric(temp$auc)
  
  rf.pred = predict(rf.fit, c, type = "prob", 1)[,1]
  
  temp=roc(labels_test_c, rf.pred)
  
  total_rfc[i/step] = as.numeric(temp$auc)
  
  svm.pred = predict(svm.fit, d, probability=TRUE, decision.values = TRUE)
  
  temp=roc(labels_test_d, attributes(svm.pred)$probabilities[,1])
  
  total_svm_d[i/step] = as.numeric(temp$auc)
  
  rf.pred = predict(rf.fit, d, type = "prob", 1)[,1]
  
  temp=roc(labels_test_d, rf.pred)
  
  total_rfd[i/step] = as.numeric(temp$auc)
  
  svm.pred = predict(svm.fit, m, probability=TRUE, decision.values = TRUE)
  
  temp=roc(labels_test_m, attributes(svm.pred)$probabilities[,1])
  
  total_svm_m[i/step] = as.numeric(temp$auc)
  
  rf.pred = predict(rf.fit, m, type = "prob", 1)[,1]
  
  temp=roc(labels_test_m, rf.pred)
  
  total_rfm[i/step] = as.numeric(temp$auc)
  
  svm.pred = predict(svm.fit, p, probability=TRUE, decision.values = TRUE)
  
  temp=roc(labels_test_p, attributes(svm.pred)$probabilities[,1])
  
  total_svm_p[i/step] = as.numeric(temp$auc)
  
  rf.pred = predict(rf.fit, p, type = "prob", 1)[,1]
  
  temp=roc(labels_test_p, rf.pred)
  
  total_rfp[i/step] = as.numeric(temp$auc)
  
  svm.pred = predict(svm.fit, s, probability=TRUE, decision.values = TRUE)
  
  temp=roc(labels_test_s, attributes(svm.pred)$probabilities[,1])
  
  total_svm_s[i/step] = as.numeric(temp$auc)
  
  rf.pred = predict(rf.fit, s, type = "prob", 1)[,1]
  
  temp=roc(labels_test_s, rf.pred)
  
  total_rfs[i/step] = as.numeric(temp$auc)
  
  x_data=as.data.frame(cbind(x.tr,labels_[tr.idx]))
  
  names(x_data)[8421] <- "labels"
  
  n <- names(x_data)
  f <- as.formula(paste("labels~", paste(n[!n %in% "labels"], collapse = " + ")))
  
  gbm.fit <- gbm(f,data=x_data, 
                 distribution="bernoulli", 
                 keep.data=TRUE,
                 n.trees=1000,
                 shrinkage=0.05, 
                 interaction.depth=3,
                 bag.fraction = 0.8,
                 train.fraction = 1.0, 
                 n.minobsinnode = 10,
                 cv.folds = 3, 
                 class.stratify.cv = TRUE,
                 verbose=FALSE, 
                 n.cores=1) 
  
  gbm.pred=predict(gbm.fit, as.data.frame(c), type = "response", n.trees=1000)
  temp=roc(labels_test_c, gbm.pred)
  total_gbm_c[i/step] = as.numeric(temp$auc)
  
  gbm.pred=predict(gbm.fit, as.data.frame(d), type = "response", n.trees=1000)
  temp=roc(labels_test_d, gbm.pred)
  total_gbm_d[i/step] = as.numeric(temp$auc)
  
  gbm.pred=predict(gbm.fit, as.data.frame(m), type = "response", n.trees=1000)
  temp=roc(labels_test_m, gbm.pred)
  total_gbm_m[i/step] = as.numeric(temp$auc)
  
  gbm.pred=predict(gbm.fit, as.data.frame(p), type = "response", n.trees=1000)
  temp=roc(labels_test_p, gbm.pred)
  total_gbm_p[i/step] = as.numeric(temp$auc)
  
  gbm.pred=predict(gbm.fit, as.data.frame(s), type = "response", n.trees=1000)
  temp=roc(labels_test_s, gbm.pred)
  total_gbm_s[i/step] = as.numeric(temp$auc)
  
  
  #Tests

#Test NN
  
  z_data = as.data.frame(cbind(x.te,labels_[te.idx]))
  colnames(z_data)[dim(z_data)[2]] <- "labels_test"
  
  test.nn = compute(nn,z_data[,1:dim(z_data)[2]-1])
  test.nn_ = test.nn$net.result*(max(z_data$labels_test)-min(z_data$labels_test))+min(z_data$labels_test)
  test2.r = (z_data$labels_test)*(max(z_data$labels_test)-min(z_data$labels_test))+min(z_data$labels_test)
  
  temp = roc(as.numeric(test2.r), as.numeric(test.nn_))
  
  test_nn[i/step]=as.numeric(temp$auc)

#Test LM
    
  test.lm <- predict(lm.fit,z_data[,1:dim(z_data)[2]-1], type="response")
  
  temp=roc(as.numeric(z_data$labels_test), as.numeric(test.lm))
  
  test_lm[i/step] = as.numeric(temp$auc)
  
#Test RF
  
  rf.pred = predict(rf.fit, x.te, type = "prob", 1)[,1]
  
  temp=roc(y.te, rf.pred)
  
  test_rf[i/step] = as.numeric(temp$auc)
  
#Test GBM
  
  gbm.pred=predict(gbm.fit, as.data.frame(x.te), type = "response", n.trees=1000)
  temp=roc(y.te, gbm.pred)
  test_gbm[i/step] = as.numeric(temp$auc)
  
#Test SVM
  
  svm.pred = predict(svm.fit, x.te, probability=TRUE, decision.values = TRUE)
  
  temp=roc(y.te, attributes(svm.pred)$probabilities[,1])
  
  test_svm[i/step] = as.numeric(temp$auc)
  
  ###
  
  
}

library(ggplot2)
training_set_size=1:cycles
df <- data.frame(training_set_size=rep(training_set_size,20), AUC=c(total_rfc, total_rfd, total_rfm, total_rfp, total_rfs, test_rf, total_gbm_c, total_gbm_d, total_gbm_m, total_gbm_p, total_gbm_s, test_gbm, total_svm_c, total_svm_d, total_svm_m, total_svm_p, total_svm_s, test_svm, test_lm, test_nn), Set=c(rep("RF-C", cycles), rep("RF-D", cycles), rep("RF-M", cycles), rep("RF-P", cycles), rep("RF-S", cycles), rep("TEST-RF", cycles), rep("GBM-C", cycles), rep("GBM-D", cycles), rep("GBM-M", cycles), rep("GBM-P", cycles), rep("GBM-S", cycles), rep("TEST-GBM", cycles),rep("SVM-C", cycles), rep("SVM-D", cycles), rep("SVM-M", cycles), rep("SVM-P", cycles), rep("SVM-S", cycles), rep("TEST-SVM", cycles), rep("TEST-LM", cycles), rep("TEST-NN", cycles)))
ggplot(df, aes(x=training_set_size, y=AUC, color=Set)) + geom_line() + geom_point() + scale_x_discrete(limits=c("1","2","3","4","5","6","7","8","9"),labels=c("10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%")) #theme(axis.text.x = element_blank(), axis.text.y = element_blank())
ggplotly()

#https://cran.r-project.org/web/packages/Metrics/Metrics.pdf

