nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R C_elegans > logfile_CE2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R D_melanogaster > logfile_DM2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R M_musculus > logfile_MM2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R S_cerevisiae > logfile_SC2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R S_pombe > logfile_SP2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R H_sapiens > logfile_HS2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R M_musculus_KABIR > logfile_Kabir2.txt &
nohup /usr/local/bin/Rscript Caret_FS_REAL_OGEE.R H_sapiens_GUO > logfile_Guo2.txt &
