#!/bin/sh
~/miniconda3/bin/udocker run --rm -i --volume=/home/tulio:/var/spool/glycos:rw --user=1044:1001  --workdir=/var/spool/glycos netnglyc:latest $1 | grep "+" | awk -F "\t"  '{print $7}'  | sort |  uniq -c | awk '{print $1"\t"$2}' > $2
