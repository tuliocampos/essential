#!/bin/bash
#/usr/local/bin/Rscript insertData.R
/usr/local/bin/Rscript insertfeatures.R
/usr/local/bin/Rscript insertfeaturesDNA.R
/usr/local/bin/Rscript Wormbase.R
/usr/local/bin/Rscript MGI.R
/usr/local/bin/Rscript FlyBase.R
/usr/local/bin/Rscript PomBase.R
/usr/local/bin/Rscript ZFIN.R
/usr/local/bin/Rscript SGD.R
/usr/local/bin/Rscript GenomeCRISPR.R
/usr/local/bin/Rscript SeedGenes.R
