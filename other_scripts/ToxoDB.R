###ToxoDB

library(biomaRt)
library(RMySQL)

mydb = dbConnect(MySQL(), user='tulio', password='Rstudio18', dbname='essential', host='localhost')
list_genes=dbGetQuery(mydb, "select gene_id from essential_genes where species_id='Toxoplasma gondii - GT1'")
dbDisconnect(mydb)

write.table(list_genes, file="list_genes.txt", sep="", row.names = FALSE, quote=FALSE)

system("fgrep -f list_genes.txt ToxoDB_data.txt > ToxoDB.matches")

toxodb=read.table("ToxoDB.matches", sep="\t" , fill=TRUE)

head(toxodb)

for (i in 1:nrow(toxodb)) {
  print (as.character(toxodb[i,1]))
  mydb = dbConnect(MySQL(), user='tulio', password='Rstudio18', dbname='essential', host='localhost')
  dbSendQuery(mydb, paste("insert into essential_phenotype_toxodb (gene_id,profile_set,dataset,log2_fc,standard_error,fdr) VALUES ('",as.character(toxodb[i,1]),"','",as.character(gsub("\\'", "",toxodb[i,2])),"','",as.character(toxodb[i,3]),"',",toxodb[i,6],",",toxodb[i,7],",",toxodb[i,8],")",sep=""))
  dbDisconnect(mydb)
}

