#!/usr/bin/env python
import os
import sys

p=open(sys.argv[1],'r')
#p = open(tmp,"r")
result = {'mw':0,'charge':0,'iep':0,'Tiny':0,'Small':0,'Aliphatic':0,'Aromatic':0,'Non-polar':0,'Polar':0,'Charged':0,'Basic':0,'Acidic':0}
DayhoffStat = {'A':0,'B':0,'C':0,'D':0,'E':0,'F':0,'G':0,'H':0,'I':0,'J':0,'K':0,'L':0,'M':0,'N':0,'O':0,'P':0,'Q':0,'R':0,'S':0,'T':0,'U':0,'V':0,'W':0,'X':0,'Y':0,'Z':0}
lines = p.readlines()
p.close()

result['mw']     = lines[2].split()[3]
result['charge'] = lines[3].split()[7]
result['iep']    = lines[4].split()[3]
for x in range(38,47):
   result[lines[x].split()[0]] = lines[x].split()[3]
for y in range(10,36):
   DayhoffStat[lines[y].split()[0]] = lines[y].split()[5]
del DayhoffStat['J']   #
del DayhoffStat['O']   # These are never used, but inserted just to make the code simple
del DayhoffStat['U']   #

#print (dict.items(result))
#print (dict.keys(result),dict.keys(DayhoffStat))
print (dict.values(result),dict.values(DayhoffStat))
