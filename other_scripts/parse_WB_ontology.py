#!/usr/bin/env python
import os
import sys

ide=''
name=''
defi=''
isa=''

f=open('phenotype_ontology.WS264.obo','r')
for line in f:
    if line.startswith('[Term]'):
      if ide!='':
       print (ide+"\t"+name+"\t"+defi+"\t"+isa)
      ide=''
      name=''
      defi=''
      isa=''
    if line.startswith('id:'):
     ide=ide+";"+line.rstrip('\r\n')[4:]
    if line.startswith('name:'):
     name=name+";"+line.rstrip('\r\n')[6:]
    if line.startswith('def:'):
     defi=defi+";"+line.rstrip('\r\n')[5:]
    if line.startswith('is_a:'):
     isa=isa+";"+line.rstrip('\r\n')[6:]
if ide!='':
       print (ide+"\t"+name+"\t"+defi+"\t"+isa)
